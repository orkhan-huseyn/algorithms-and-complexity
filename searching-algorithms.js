
// good old linear search
// O(n)
function linearSearch(array, target) {
	for (let i = 0; i < array.length; i++) {
		if (array[i] === target) {
			return i;
		}
	}
	return -1;
}

// O(log n)
// binary search algorithm (iterative and recursive)

// 13
// [1, 4, 5, 7, 8, 10, 13, 18, 23, 55]

function binarySearch(array, target) {
	let left = 0;
	let right = array.length - 1;

	while (left <= right) {
		const middle = Math.floor((left + right) / 2);

		if (array[middle] === target) {
			return middle;
		}

		if (target > array[middle]) {
			left = middle + 1;
		} else {
			right = middle - 1;
		}
	}

	return -1;
}


function binarySearchRecursive(array, target, left, right) {
	if (right < left) {
		return -1;
	}

	const middle = Math.floor((left + right) / 2);
	if (array[middle] === target) {
		return middle;
	}

	if (target > array[middle]) {
		return binarySearchRecursive(array, target, middle + 1, right);
	} else {
		return binarySearchRecursive(array, target, left, middle - 1);
	}
}