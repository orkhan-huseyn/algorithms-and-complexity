
// why can't we measure time?

// time complexity - O(1)
function pickOne(array) {
	const randomIndex = random(0, array.length);
	return array[randomIndex];
}

// time complexity - O(n)
// space complexity - O(1)
function includes(array, target) {
	for (let element of array) {
		if (target === element) {
			return true;
		}
	}
	return false;
}


// time complexity - O(n)
// space complexity - O(n)
function map(array, callback) {
	let result = [];
	for (let i = 0; i < array.length; i++) {
		result.push(callback(array[i], i, array));
	}
	return result;
}

// common complexity functions

// examples

// time complexity - O(n)
function logUpToN(array) {
	for (let i = 0; i < Math.max(array.length, 10); i++) {
		console.log(array[i]);
	}
}

// time complexity - O(1)
function logUpTo10(array) {
	for (let i = 0; i < Math.min(array.length, 10); i++) {
		console.log(array[i]);
	}
}

// time complexity - O(n**2)
// space complexity - O(1)
function twoSum(nums, target) {
    for (let i = 0; i < nums.length; i++) {
        for (let j = i + 1; j < nums.length; j++) {
            if (nums[i] + nums[j] === target) {
                return [i, j];
            }
        }
    }
}

// time complexity - O(n)
// space complexity - O(n)
function twoSum(nums, target) {
    const map = {};
    for (let i = 0; i < nums.length; i++) {
        const curr = nums[i];
        
        if (curr in map) {
            return [map[curr], i];
        }
        
        const sub = target - curr;
        map[sub] = i;
    }
}