
// swapping algorithm
// how to swap to variables?
let a = 12;
let b = 21;

let temp = a;
a = b;
b = temp;

// bubble sort
// compare j to j + 1 and swap if j > j + 1
// O(n ** 2)
function bubleSort(array, compareFn) {
	for (let i = 0; i < array.length; i++) {
		for (let j = 0; j < array.length - 1; j++) {
			if (array[j] > array[j + 1]) {
				let temp = array[j];
				array[j] = array[j + 1];
				array[j + 1] = temp;
			}
		}
	}
	return array;
}

// selection sort
// find minimum and bring it to beginning

// merge sort 
// time O(n log n)
// space O(n)
// https://cs.slides.com/colt_steele/intermediate-sorting-algorithms#/5

// quick sort time O(n log n) space O(1)